#include<stdio.h>
#include<math.h>

int main()
{
    int number,t;
    printf("Input number:");
    scanf("%d",&number);
    t=pow(number,0.5);
    printf("Factors for %d:\n",number);
    for(int i=1;i<=t;i++)
    {
        if(number % i==0){
            printf(" %d * %d = %d \n",i,number/i,number);
        }
    }
    return 0;
}
