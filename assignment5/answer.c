/*

relationship between profit and ticket price is:
ticket price = x
participants = y

y= -4x +180
profit= -4x*x +192x -1040

*/
#include<stdio.h>

float profit(float ticket_price)
{
    float profit=0.0;
    profit = -4.0*ticket_price*ticket_price + 192.0*ticket_price - 1040.0;
    return profit;
}

int participants(float ticketPrice)
{
    int partcipate=0;
    partcipate= -4 *(int)ticketPrice +180;
    return partcipate;
}

float getMaxValue()
{
// rerurning the ticket price that which can give the maximum profit
    float maxTicket=0.0;
    maxTicket=192.0/8.0;
    return maxTicket;
}

int main()
{
    float ticketprice,Profit;
    int participate;
    ticketprice=getMaxValue();
    participate= participants(ticketprice);
    Profit=profit(ticketprice);
    printf("Most profitable ticket price: Rs. %0.2f\n",ticketprice);
    printf("Amount of participants: %d \n",participate);
    printf("Profit: Rs. %0.2f\n",Profit);

    return 0;
}
