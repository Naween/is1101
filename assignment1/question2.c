// Author - Naween Pasindu
// Index Num-202041
#include<stdio.h>

int main()
{
    const  double pie= 22.0/7;
    float radius=0;
    double area;
    printf("Input Radius:");
    scanf("%f",&radius);
    area = pie*(double)radius*(double)radius;
    printf("Area of circle is : %lf units.",area);
    return 0;
}
