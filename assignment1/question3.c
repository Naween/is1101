// Author - Naween Pasindu
// Index Num-202041
#include<stdio.h>

int main()
{
    int num1,num2,temp;
    printf("Input number 1:");
    scanf("%d",&num1);
    printf("Input number 2:");
    scanf("%d",&num2);
    printf("previous values of number 1 and number 2 is : %d and %d\n",num1,num2);
    temp=num1;
    num1=num2;
    num2=temp;
    printf("previous values of number 1 and number 2 is : %d and %d",num1,num2);
    return 0;
}
