FROM gcc
MAINTAINER Naween Pasindu
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "htnaweenpasindu@gmail.com"
RUN git config --global user.name "Naween"
