#include<stdio.h>

struct student
{
    char FirstName[20];
    char Subject[10];
    int marks;
};

void instertData(struct student *st);

int main()
{
    struct student data[5];
    for(int i=0; i<sizeof(data)/sizeof(struct student); i++)
    {
        instertData(&data[i]);
    }
    printf("---------------------------\n");
    for(int i=0; i<sizeof(data)/sizeof(struct student); i++)
    {
        printf("Data of student- %d\n",i+1);
        printf("%s\n",data[i].FirstName);
        printf("%s\n",data[i].Subject);
        printf("%d\n",data[i].marks);
        printf("---------------------------\n");
    }
    return 0;
}

void instertData(struct student *st)
{
    printf("Enter first name:");
    scanf(" %[^\n]s",&st->FirstName);
    printf("Enter subject name:");
    scanf(" %[^\n]%*c",&st->Subject);
    printf("Enter subject marks:");
    scanf("%d",&st->marks);
}

