#include<stdio.h>
#include<string.h>
int main()
{
    int check=0,mr1=0,mc1=0,mr2=0,mc2=0;
    printf("Enter the number of rows in matrix     1:");
    scanf("%d",&mr1);
    printf("Enter the number of columns in matrix  1:");
    scanf("%d",&mc1);
    printf("Enter the number of rows in matrix     2:");
    scanf("%d",&mr2);
    printf("Enter the number of columns in matrix  2:");
    scanf("%d",&mc2);

    int matrix1[mr1][mc1],matrix2[mr2][mc2];
    printf("Enter matrix 1 data:\n");
    for(int i=0; i<mr1; i++)
    {
        for(int j=0; j<mc1; j++)
        {
            printf("enter the data of %d rows th %d th column:",i+1,j+1);
            scanf("%d",&matrix1[i][j]);
        }
    }
    printf("matrix 1:\n");
    for(int i=0; i<mr1; i++)
    {
        printf("|");
        for(int j=0; j<mc1; j++)
        {
            printf("%4d",matrix1[i][j]);
        }
        printf("%4c|\n",'\0');
    }
    printf("\nEnter matrix 2 data:\n");
    for(int i=0; i<mr2; i++)
    {
        for(int j=0; j<mc2; j++)
        {
            printf("enter the data of %d rows th %d th column:",i+1,j+1);
            scanf("%d",&matrix2[i][j]);
        }
    }
    printf("matrix 2:\n");
    for(int i=0; i<mr2; i++)
    {
        printf("|");
        for(int j=0; j<mc2; j++)
        {
            printf("%4d",matrix2[i][j]);
        }
        printf("%4c|\n",'\0');
    }

    if(mr1==mr2 && mc1==mc2)
    {
        int output[mr1][mc1];
        for(int i=0; i<mr1; i++)
        {
            for(int j=0; j<mc1; j++)
            {
                output[i][j]=matrix1[i][j]+matrix2[i][j];
            }
        }
        printf("\nmatrix 1 + matrix 2  :\n");
        for(int i=0; i<mr2; i++)
        {
            printf("|");
            for(int j=0; j<mc2; j++)
            {
                printf("%4d",output[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Can't add matrix 1 and matrix 2.\n");
    }
    if(mc1==mr2)
    {
        int output2[mr1][mc2];
        for(int i=0; i<mr1; i++)
        {
            for(int j=0; j<mc2; j++)
            {
                output2[i][j]=0;
            }
        }
        for (int i = 0; i < mr1; i++)
        {
            for (int j = 0; j < mc2; j++)
            {
                for (int k = 0; k < mc1; k++)
                {
                    output2[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        printf("\nmatrix 1 * matrix 2  :\n");
        for(int i=0; i<mr1; i++)
        {
            printf("|");
            for(int j=0; j<mc2; j++)
            {
                printf("%4d",output2[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Cannot multiply matrix 1 and matrix 2.");
    }
    return 0;
}
